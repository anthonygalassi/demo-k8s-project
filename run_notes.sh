# With this repo one is able to setup an eks cluster and also 
# install an package using helm via the helm chart folder

# usage
# After cloning the package 
pushd cluster/
# initialize and apply terraform 
terraform init 
yes | terraform apply
# update kubeconfig with aws console 
aws eks --region us-east-1 update-kubeconfig --name my-cluster
sleep 20m


# Apply helm packages
popd
pushd helm-chart/
terraform init
yes | terraform apply
sleep 5m
popd


# instructions and reading used to proform setup of prometheus were 
# taken from the sources below, read through the first link and move onto
# the second to setup the metrics server, then complete the instructions
# from the first link
# https://computingforgeeks.com/deploying-prometheus-on-eks-kubernetes-cluster/
# https://computingforgeeks.com/install-kubernetes-metrics-server-on-eks-cluster/


# install k8's prometheus monitoring
export KUBECONFIG=~/.kube/config
# add check to see if nodes are up

# install metrics 
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.7/components.yaml

# set up metrics server
kubectl get deployment metrics-server -n kube-system

# check to see if server is up by collecting output from this command
kubectl get apiservice v1beta1.metrics.k8s.io -o yaml

# congrats you can view ful metrics with this command
#kubectl get --raw /metrics

# create namespace monitoring?
kubectl create namespace monitoring

# this does something
kubectl get sc

# install repo for monitoring, this already existed for me
helm repo add stable https://kubernetes-charts.storage.googleapis.com

# install some more prometheus stuff
helm install prometheus stable/prometheus \
    --namespace monitoring \
    --set alertmanager.persistentVolume.storageClass="gp2",server.persistentVolume.storageClass="gp2"

# confirm PV and PVC are created
kubectl get pv -n monitoring
kubectl get pvc -n monitoring

# access prometheus on EKS Cluster
kubectl get all -n monitoring

# collect prometheus server url
export POD_NAME=$(kubectl get pods --namespace monitoring -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")

# Use Kubernetes port forwarding feature to access Prometheus Server.
kubectl --namespace monitoring port-forward $POD_NAME 9090

# TODO add litmus to project, need to read setup/introduction here:
# https://docs.litmuschaos.io/docs/getstarted/

# set up grafana
# add the follwing to a yaml named grafana.yaml
"datasources:
  datasources.yaml:
    apiVersion: 1
    datasources:
    - name: Prometheus
      type: prometheus
      url: http://prometheus-server.prometheus.svc.cluster.local
      access: proxy
      isDefault: true"

# run these commands next 
kubectl create namespace grafana
helm install grafana stable/grafana \
    --namespace grafana \
    --set persistence.storageClassName="gp2" \
    --set persistence.enabled=true \
    --set adminPassword='EKS!sAWSome' \
    --values grafana.yaml \
    --set service.type=LoadBalancer

# after the above is setup you can obtain your address for your grafana interface via
kubectl get svc -n grafana grafana -o jsonpath='{.status.loadBalancer.ingress[0].hostname}

